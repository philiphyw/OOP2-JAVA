package exceptionhandling;

public class ExceptionHanding {

	public static void main(String[] args) {
//		try {
//			System.out.println("Fisrt line of the program.");
//			//ArithmeticException

		// int result = 15 / 0;

		// NullPointerException

//			String name = null;;
//			System.out.println(name.length());

		// IndexOutOfBoundsException
//			String [] nameArray = new String[]{"Jim","Tom"};
//			System.out.println(nameArray[20].toString());
//			
//			System.out.println("Last line of the program");
//		} catch (ArithmeticException e) {
//			System.out.println("Catch an ArithmeticException:" + e.getMessage());
//		} catch (NullPointerException e) {
//			System.out.println("Catch a NullPointerException:" + e.getMessage());
//		}catch (IndexOutOfBoundsException e) {
//			System.out.println("Catch an IndexOutOfBoundsException:" + e.getMessage());
//		}

		System.out.println("Now we're gonna withdraw -3");
		withdrawBalance(-3);
		System.out.println("Now we're gonna withdraw 500");
		withdrawBalance(500);

	}

	public static void withdrawBalance(int withdrawAmount) {
		try {
			if (withdrawAmount < 0) {
				throw new IllegalArgumentException("Exception: Withdraw amount must NOT less than zero.");
			}
			System.out.printf("%n%nYou have withdrawn %d dollars.", withdrawAmount);

		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}
}