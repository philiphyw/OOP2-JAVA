package exam;

public class SmsUser extends User {
	private String phoneNumber;

	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		this.setPhoneNumber(phoneNumber);
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		String[] arr = phoneNumber.split("");
		try {
		for (int i = 0; i < arr.length; i++) {
			Integer.parseInt(arr[i]);
		}
		this.phoneNumber=phoneNumber;
		}catch(IllegalArgumentException e) {
			throw new IllegalArgumentException(this.getFirstName() +" "+ this.getLastName() + "'s phone number should only contains digits: " + phoneNumber);
		}
		
	}

}
