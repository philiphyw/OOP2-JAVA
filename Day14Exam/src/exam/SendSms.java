package exam;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendSms implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {

		boolean result = true;
		
		// Implement the body to validate if the user has
		// a correct phone number 10 digit
		//Bonus if you use regex for the phone number
		//and if it is not validate then throw IllegalArgumentException
		//with a proper message 

		

		
		if(!validatePhoneNumber(((SmsUser) sender).getPhoneNumber())) {
			result = false;
			throw new IllegalArgumentException(sender.getFirstName() + " " + sender.getLastName() + "'s phone number must be a 10 digits number: " + ((SmsUser) sender).getPhoneNumber());
		}
		
		if(!validatePhoneNumber(((SmsUser) receiver).getPhoneNumber())) {
			result = false;
			throw new IllegalArgumentException(receiver.getFirstName() + " " + receiver.getLastName()+"'s phone number must be a 10 digits number: " + ((SmsUser) receiver).getPhoneNumber());
		}
		

		// Check if the message is not empty
		if(body.isEmpty()) {
			result = false;
			throw new IllegalArgumentException("The message body should NOT be empty");
		}
		
		char[] charArr = body.toCharArray();
		if (charArr.length>160) {
			result = false;
			throw new IllegalArgumentException("The message body should NOT longer than 160 characters: " + body);
		}
		
		// check if it does not contain(& or # or @ in the message body
		// and if it contains throw IllegalArgumentException
		
		if(body.contains("&")  ||body.contains("#") || body.contains("@")) {
			result = false;
			throw new IllegalArgumentException("The message body contains illegal character(&, #, @): " + body);
		}
		
		
		return result;

	}

	@Override
	public void sendMessage(Message message) {
		try {
		BufferedWriter bw = new BufferedWriter(new FileWriter("src/exam/sms.txt")) ;
		bw.write("From: " + ((SmsUser)message.getSender()).getPhoneNumber());
		bw.write("\n");
		bw.write("To: " + ((SmsUser)message.getReceiver()).getPhoneNumber());
		bw.write("\n");
		bw.write("Body: " +message.getBody());
		bw.close();
		}catch(IOException e) {
			System.out.println("This is an exception while writing the file SMS.txt." + e.getMessage());
		}

	}

	
	
	
	private static boolean validatePhoneNumber(String phoneNumber) {
		
			boolean result = true;
		
			String [] strArr = phoneNumber.split("");
			if( strArr.length != 10) {
				result = false;
			}
			
			
			int findMatch = 0;

			Pattern checkRegex = Pattern.compile("\\D");
			Matcher regexMatcher = checkRegex.matcher(phoneNumber);
			while (regexMatcher.find())
				findMatch++;

			if (findMatch>0) {
				result = false;
				
			}
		

		return result;
	}
	
	

}
