package exam;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {

		try {
		
		// Create a list of Messages
		List<Message> listOfMessages = new ArrayList<Message>();

		// Create An EmailMessage instance
		EmailMessage email = new EmailMessage(
				new EmailUser("Judy", "Foster", new Address("Main Street", 1), "ab@g.com"),
				new EmailUser("Betty", "Beans", new Address("Second Street", 2), "vr@g.com"), "This is one email");

		// Create A SmsMessage instance
	
		SmsMessage smsMessage = new SmsMessage(new SmsUser("Judy", "Foster", new Address("Main Street", 1), "0987654321"),
				new SmsUser("Betty", "Beans", new Address("Second Street", 2), "1234567890"), "This is one SMS");
		
		// add the messages into the list
		listOfMessages.add(email);
		listOfMessages.add(smsMessage);

		
		
		
		// iterate through the list and call sendMessage if validate Message return
		// true, other wise print a proper message to show that message is invalid
		for (int i = 0; i < listOfMessages.size(); i++) {
			Message message = listOfMessages.get(i);
			System.out.printf("\n\nProcessing the message %d :\n",i+1);
				if (message instanceof EmailMessage) {

					SendEmail se = new SendEmail();
					if (se.validateMessage(message.getSender(), message.getReceiver(), message.getBody())) {
						se.sendMessage(listOfMessages.get(i));
						System.out.println("Email sent");
					}

				} else if (message instanceof SmsMessage) {

					SendSms ss = new SendSms();
					if (ss.validateMessage(message.getSender(), message.getReceiver(), message.getBody())) {
						ss.sendMessage(listOfMessages.get(i));
						System.out.println("SMS sent");
					}
				}
			}
		}catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
			
		}
	}
}