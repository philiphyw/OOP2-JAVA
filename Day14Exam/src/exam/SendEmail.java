package exam;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendEmail implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {

		boolean result = true;
		
		// validate if the email addresses are correct
		// it needs to contain(@ and .) only one time in the email address

		
		String[] uniqueCharacter = {"\\.","\\@"};
		
		if(!validateEmailAddress(((EmailUser) sender).getEmailAddress(), uniqueCharacter)) {
			result = false;
			throw new IllegalArgumentException(sender.getFirstName() + " " + sender.getLastName() + " 's email address is invalid, it should contain @ & . only one time: " + ((EmailUser) sender).getEmailAddress());
		}
		
		if(!validateEmailAddress(((EmailUser) receiver).getEmailAddress(), uniqueCharacter)) {
			result = false;
			throw new IllegalArgumentException(receiver.getFirstName() + " " + receiver.getLastName() + " 's email address is invalid, it should contain @ & . only one time: " + ((EmailUser) receiver).getEmailAddress());
		}
		

		// Check if the message is not empty
		if(body.isEmpty()) {
			result = false;
			throw new IllegalArgumentException("Message body must NOT be empty.");
		}
		
		// check if it does not contain(^ or * or !) in the message body
		// and if it contains throw IllegalArgumentException
		
		if(body.contains("^")  ||body.contains("*") || body.contains("!")) {
			result = false;
			throw new IllegalArgumentException("The message body contains illegal character(^, *, !): " + body);
		}
		
		
		return result;

	}

	@Override
	public void sendMessage(Message message) {

		try {
		BufferedWriter bw = new BufferedWriter(new FileWriter("src/exam/email.txt")) ;
		bw.write("From: " + ((EmailUser)message.getSender()).getEmailAddress());
		bw.write("\n");
		bw.write("To: " + ((EmailUser)message.getReceiver()).getEmailAddress());
		bw.write("\n");
		bw.write("Body: " +message.getBody());
		bw.close();
		}catch(IOException e) {
			System.out.println("This is an exception while writing the file email.txt." + e.getMessage());
		}

	}

	
	
	
	private static boolean validateEmailAddress(String emailAddress, String... regex) {
		
		boolean result = true;
		
		for (int i = 0; i < regex.length; i++) {
			int findMatch = 0;

			Pattern checkRegex = Pattern.compile(regex[i]);
			Matcher regexMatcher = checkRegex.matcher(emailAddress);
			while (regexMatcher.find())
				findMatch++;

			if (findMatch != 1) {
				result = false;
			}
		}

		return result;
	}
	
	

}
