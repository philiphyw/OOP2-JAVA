package day03homework;

import java.util.Scanner;

public class App {
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		// Assignment 1
		System.out.println("Assignment 1:");
		System.out.println("-------------------------------------------------");
		Shape s1 = new Square();
		noAbsShape s2 = new noAbsSquare();

		System.out.println("Please input the length: ");

		double length = in.nextDouble();

		System.out.printf(
				"Square one extends abstract class, its length is %.2f, its area is %.2f, and its perimeter is %.2f.%n",
				length, s1.getArea(length), s1.getPerimeter(length));
		System.out.printf(
				"Square two extends non-abstract class, its length is %.2f, its area is %.2f, and its perimeter is %.2f.%n",
				length, s2.getArea(length), s2.getPerimeter(length));

		System.out.println();

		Shape c1 = new Circle();
		noAbsShape c2 = new noAbsSquare();

		System.out.println("Please input the radius: ");

		double radius = in.nextDouble();

		System.out.printf(
				"Square one extends abstract class, its radius is %.2f, its area is %.2f, and its perimeter is %.2f.%n",
				radius, c1.getArea(radius), c1.getPerimeter(radius));
		System.out.printf(
				"Square two extends non-abstract class, its radius is %.2f, its area is %.2f, and its perimeter is %.2f.%n",
				radius, c2.getArea(radius), c2.getPerimeter(radius));

		System.out.println();
		System.out.println("The reason to use abstraciton is: ");
		System.out.println(
				"Since methods in children classes are various,and parent class can NOT provide a unique method to calculate result for all chiildre.");
		System.out.println(
				"So parent class should just use abstract method to require children classes to provide their own solutions by overriding the abstract method");

		System.out.printf("-------------------------------------------------%n%n%n%n");

		// Assignment 2
		System.out.println("Assignment 2:");
		System.out.println("-------------------------------------------------");
		Animals cat1 = new Cats();
		Animals dog1 = new Dogs();

		System.out.println("This is a cat, here is how it sounds: ");
		cat1.Sound();
		System.out.println();
		System.out.println("This is a dog, here is how it sounds: ");
		dog1.Sound();

		System.out.printf("-------------------------------------------------%n%n%n%n");
		
		
		// Assignment 3
		System.out.println("Assignment 3:");
		System.out.println("-------------------------------------------------");
		Marks a = new A(90,78,85);
		Marks b = new B(99,60,70,88);


		System.out.println("This is the percentage for A :");
		a.getPercentage();
		System.out.println();
		System.out.println("This is the percentage for B :");
		b.getPercentage();

		System.out.printf("-------------------------------------------------%n%n%n%n");

	}

}

//Assignment 1:
abstract class Shape {
	protected double length;

	abstract double getArea(double length);

	abstract double getPerimeter(double length);
}

class Square extends Shape {

	@Override
	public double getArea(double length) {

		return length * length;
	}

	@Override
	public double getPerimeter(double length) {

		return length * 4;
	}

}

class Circle extends Shape {
	protected double radious;

	@Override
	public double getArea(double radius) {

		return Math.pow(radius, 2) * 3.14;
	}

	@Override
	public double getPerimeter(double radius) {

		return radius * 2 * 3.14;
	}

}

class noAbsShape {

	protected double length;

	public double getArea(double length) {
		return 0;
	}

	public double getPerimeter(double length) {
		return 0;
	}

}

class noAbsSquare extends noAbsShape {

	public double getArea(double length) {
		return length * length;
	}

	public double getPerimeter(double length) {

		return length * 4;
	}
}

class noAbsCircle extends noAbsShape {
	protected double radious;

	public double getArea(double radius) {

		return Math.pow(radius, 2) * 3.14;
	}

	public double getPerimeter(double radius) {

		return radius * 2 * 3.14;
	}

}

//Assignment 2:

abstract class Animals {
	public abstract void Sound();
}

class Cats extends Animals {
	@Override
	public void Sound() {
		System.out.println("Meow!");
	}
}

class Dogs extends Animals {
	@Override
	public void Sound() {
		System.out.println("Woof!");
	}
}

//Assignment 3:
abstract class Marks {
	abstract void getPercentage();
}

class A extends Marks {
	private int[] subjects = new int[3];

	A(int subject1, int subject2, int subject3) {
		this.subjects[0] = subject1;
		this.subjects[1] = subject2;
		this.subjects[2] = subject3;
	}

	@Override
	public void getPercentage() {
		CalArray.percentArray(this.subjects);
	}
}


class B extends Marks {
	private int[] subjects = new int[4];

	B(int subject1, int subject2, int subject3, int subject4) {
		this.subjects[0] = subject1;
		this.subjects[1] = subject2;
		this.subjects[2] = subject3;
		this.subjects[3] = subject3;
	}

	@Override
	public void getPercentage() {
		CalArray.percentArray(this.subjects);
	}
}



class CalArray {
	public static int sumArray(int[] arr) {
		int sum = 0;
		for (int i : arr) {
			sum += i;
		}
		return sum;
	}

	public static void percentArray(int[] arr) {
		int sum = sumArray(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.printf("The percentage of subject %d is %.2f%% %n", i + 1, (double) arr[i] * 100 / sum);
		}
	}
}
