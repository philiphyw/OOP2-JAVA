package implementComparable;

//Enable sorting by implementing Comparable. However, since we insert business logic into the class, this is a relatively bad practice.
//Should use comparator instead, see the example of teacher class.

public class Student extends Person implements Comparable<Student> {
	
	protected Double score;

	public Student(String name, long id, String birthDay, Double score) {
		super();
		this.name = name;
		this.id = id;
		this.birthDay = birthDay;
		this.score = score;
	}

	
	
	
	
	public Double getScore() {
		return score;
	}





	public void setScore(Double score) {
		this.score = score;
	}





	@Override
	public String toString() {
		return "[" + this.name + ", " + this.id+ ", " +this.birthDay+ ", " +this.score+ "]"; 
	}
	
	// by Overriding the compareTo method from the interface Comparable, the
	// Comparable.Sort methd wil use its return value to sort the elements
	@Override
//	public int compareTo(Student s) {
//		return this.name.compareTo(s.name);
//	}
	
	
	//since the .compareTo can NOT accept primitive variable, so just use If condition to return the int
	public int compareTo(Student s) {
		if (this.score == s.score) {
			return 0;
		} else if(this.score > s.score) {
			return -1;
		}else {
			return 1;
		}
	}
	
	
}