package implementComparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {

	public static void main(String[] args) {

		List<Student> studentList = new ArrayList<Student>();
		studentList.add(new Student("Jim", 12345, "1975/12/1", 90.5));
		studentList.add(new Student("Kim", 20521, "1995/2/12", 65.0));
		studentList.add(new Student("Tom", 98756, "2001/3/5", 70.0));
		studentList.add(new Student("Sam", 663591, "1987/9/23", 87.0));

		
		System.out.println("Before sorting:");
		studentList.forEach( e -> System.out.println(e));
		
		
		Collections.sort(studentList);
		
		
		System.out.println("\n\n\nBefore sorting:");
		studentList.forEach(e->System.out.println(e.toString()));
	}

	
}

