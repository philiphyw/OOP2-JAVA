package writerReader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class WriterReader {
	public void writeToFile(List<Address> addresses) throws IOException {

		BufferedWriter bw = new BufferedWriter(new FileWriter("src/resource/address.txt"));
		for (int i = 0; i < addresses.size(); i++) {
			bw.write(addresses.get(i).toString() + "\n");
		}
		// System.out.println("Finished Writing to File");
		bw.close();
	}

	public void readFromFile() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("src/resource/address.txt"));
		String line;
		try {
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			// System.out.println("Finished Reading from File");
			br.close();
		}

	}

}
