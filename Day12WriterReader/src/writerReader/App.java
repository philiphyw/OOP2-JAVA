package writerReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		//create 5 Address instances
		List<Address>addresses = new ArrayList<Address>();
		addresses.add(new Address(20, "Graham","Montreal","Quebec","H3T1D1"));
		addresses.add(new Address(123, "Deval","Montreal","Quebec","H3T2N6"));
		addresses.add(new Address(305, "Jean-Talon","Montreal","Quebec","Q3S1D4"));
		addresses.add(new Address(501, "Rockland","Montreal","Quebec","H1D1D1"));
		addresses.add(new Address(80, "Graham","Montreal","Quebec","H3T2D7"));

		//Write the addresses to the file
		System.out.println("Writing the file:\n\n");
		WriterReader wr = new WriterReader();
		try{
			wr.writeToFile(addresses);
			}catch(IOException e) {
				e.printStackTrace();
			}
		
		System.out.println("Finished Writing.\n\n");
		
		//read the file
		System.out.println("Reading the file:\n\n");
		try{wr.readFromFile();
		}catch(IOException e) {
			e.printStackTrace();
		}		
		
		
	}

}
