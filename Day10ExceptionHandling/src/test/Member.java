package test;

import java.util.Scanner;

public class Member {
	public String name;
	
	public void displayMenu() {
		System.out.printf("\n\n Welcome %s, how can we help you? ", this.name);
		System.out.println("------------------------------");
		System.out.println("1. Search for a book ");
		System.out.println("2. Borrow a book ");
		System.out.println("3. Return a book ");
		System.out.println("4. Pay penalty");
		System.out.println("5. Other..");
		System.out.println("------------------------------");

		Scanner sc = new Scanner(System.in);
		int input = sc.nextInt();

		switch (input) {
		case 1:
			searchBook();
			break;
		case 2:
			borrowBook();
			break;
		case 3:
			returnBook();
			break;
		case 4:
			payPenalty();
			break;
		case 5:
			displayMenu();
			break;

		default:
			System.out.println("Invalid menu");;
		}

	}

	private void payPenalty() {
		// TODO Auto-generated method stub
		System.out.println("Your penalty has been paid");
	}

	private void returnBook() {
	System.out.println("The book has been returned.");
		
	}

	private void borrowBook() {
		// TODO Auto-generated method stub
		
	}

	private void searchBook() {
		// TODO Auto-generated method stub
		
	}
}
