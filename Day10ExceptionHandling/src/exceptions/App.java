package exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {

		int a = 10;
		int b = 0;

		// Runtime exception(unchecked exception)
		try {
			int i = a / b;
		} catch (ArithmeticException e) {
			System.out.println(e.getMessage());
		}

		// Compile exception (checked exception)
		File myObj = new File("filename.txt");
		try {
			Scanner myReader = new Scanner(myObj);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}

		String s = "abc";
		try {
			int x = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			System.out.println("The exception happended beacase the number format " + e.getMessage());
		}

		try {
			int arr[] = new int[5];
			int c = arr[6];
		} catch (IndexOutOfBoundsException e) {
			System.err.println("index out of bound " + e.getMessage());
		} finally {
			System.out.println("I will be called all the time. no matter there's an exception or not.");
		}

		// if there'are multiple exceptions. only the first exception and the finally
		// will be executive
		try {
			int x = 12;
			
			String str = null;
			str.length();
			//below ArithmeticException will not be caught, since the NullPointerException will be caught first, and will jump to the finally section
			int y = 12 / 0;
			
			
			String badFormatString = "adfl";
			int z = Integer.parseInt(badFormatString);
			
		} catch (ArithmeticException e) {
			System.err.println("An ArithmeticException happended");
		} catch (NullPointerException e) {
			System.err.println("A NullPointerException happended");

		} catch (Exception e) {
			System.err.println("An exception happended");

		}finally {
			System.out.println("I am being called all the time.");
		}
	}
}
