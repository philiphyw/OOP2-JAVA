package exam;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class App {

	public static void main(String[] args) {
		// exam task 4
		System.out.println("Result for task 4:");
		Carpet carpet = new Carpet(3.5);
		Floor floor = new Floor(2.75, 4.0);
		Calculator calculator = new Calculator(floor, carpet);
		System.out.println("Total= " + calculator.getTotalCost());
		carpet = new Carpet(1.5);
		floor = new Floor(5.4, 4.5);
		calculator = new Calculator(floor, carpet);
		System.out.println("Total= " + calculator.getTotalCost());
		
		
		//exam task 5
		System.out.println("\n\n\nResult for task 5:");
		Bank a = new BankA(100);
		Bank b = new BankB(150);
		Bank c = new BankC(200);
		
		a.getBalance();
		b.getBalance();
		c.getBalance();
		
		
		//exam task 6
		System.out.println("\n\n\nResult for task 6:");
		Dimensions dimensions = new Dimensions(20,20,5);
		Case theCase = new Case("220B","Dell","240",dimensions);
		Monitor theMonitor = new Monitor("27inch Beast", "Acer",27,new Resolution(2540,1440));
		Motherboard theMotherboard = new Motherboard("BJ-200","Asus",4,6,"v2.44");
		PC thePC = new PC(theCase,theMonitor,theMotherboard);
		thePC.getTheCase().pressPowerButton();
		
	}

}

//exam task 4
class Floor {
	private double width;
	private double length;

	Floor() {
	}

	Floor(double length, double width) {
		this.length = (length <= 0) ? 0 : length;
		this.width = (width <= 0) ? 0 : width;
	}

	public double getArea() {
		return width * length;
	}
}

class Carpet {
	private double cost;

	Carpet() {
	}

	Carpet(double cost) {
		this.cost = (cost <= 0) ? 0 : cost;
	}

	public double getCost() {
		return this.cost;
	}

}

class Calculator {
	static DecimalFormat df = new DecimalFormat("#.####");

	private Floor floor;
	private Carpet carpet;

	Calculator() {
	}

	Calculator(Floor floor, Carpet carpet) {
		this.floor = floor;
		this.carpet = carpet;
	}

	public double getTotalCost() {
		double result = this.floor.getArea() * this.carpet.getCost();
		df.setRoundingMode(RoundingMode.UP);
		result = Double.valueOf(df.format(result));
		return result;
	}
}


//exam task 5
abstract class Bank{
	abstract void getBalance();
}


class BankA extends Bank{
	private double deposit;

	BankA(){}
    BankA(double deposit){
    	this.deposit = (deposit <=0)?0:deposit;
    	
    }
    
    @Override
    public void getBalance() {
    	System.out.println("Welcome to BankA, the balance in your account is " + this.deposit);
    }
}

class BankB extends Bank{
	private double deposit;

	BankB(){}
    BankB(double deposit){
    	this.deposit = (deposit <=0)?0:deposit;
    	
    }
    
    @Override
    public void getBalance() {
    	System.out.println("Greeting, our valuable customer of BankB, here is the balance in your account: " + this.deposit);
    }
}


class BankC extends Bank{
	private double deposit;

	BankC(){}
    BankC(double deposit){
    	this.deposit = (deposit <=0)?0:deposit;
    	
    }
    
    @Override
    public void getBalance() {
    	System.out.println("You are smart to choose BankC,let me show you your balance: " + this.deposit);
    }
}

//exam task 6
class PC{
	private Monitor theMonitor;
	private Case theCase;
	private Motherboard theMotherboard;
	
	PC(Case theCase, Monitor theMonitor,Motherboard theMotherboard){
		this.theMonitor=theMonitor;
		this.theCase = theCase;
		this.theMotherboard=theMotherboard;
	}
	
	
	public Case getTheCase() {
		return this.theCase;
	}
}

class Monitor{
	private String theModel;
	private String manufacturer;
	private double size;
	private Resolution resolution;
	
	Monitor(){}
	Monitor(String theModel, String manufacturer,double size,Resolution resolution ){
		this.theModel=theModel;
		this.manufacturer=manufacturer;
		this.size=size;
		this.resolution=resolution;
	}
	
}

class Case{
	private String theModel;
	private String manufacturer;
	private String powerSupply;
	private Dimensions dimensions;
	
	Case(){}
	Case(String theModel, String manufacturer, String powerSupply,Dimensions dimensions ){
		this.theModel=theModel;
		this.manufacturer=manufacturer;
		this.powerSupply=powerSupply;
		this.dimensions=dimensions;
	}
	
	public void pressPowerButton() {
		System.out.println("Power button pressed");
	}
	
}

class Motherboard{
	private String theModel;
	private String manufacturer;
	private int ramSlots;
	private int cardSlots;
	private String bios;
	
	Motherboard(){}
	Motherboard(String theModel, String manufacturer, int ramSlots, int cardSlots, String bios ){
		this.theModel=theModel;
		this.manufacturer=manufacturer;
		this.ramSlots=ramSlots;
		this.cardSlots=cardSlots;
		this.bios=bios;
	}
	
}

class Resolution{
	private int horizontal;
	private int vertical;
	
	Resolution(){}
	Resolution(int h, int v){
		this.horizontal = (h<=0)?0:h;
		this.vertical = (v<=0)?0:v;
		
	}
}


class Dimensions{
	private double height;
	private double width;
	private  double length;
	
	Dimensions(){}
	Dimensions(double height, double width, double length){
		this.height = (height <= 0) ? 0 : height;
		this.width = (width <= 0) ? 0 : width;
		this.length = (length <= 0) ? 0 : length;
	}
}