package day08interface;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.StringJoiner;

public class Company {

	public static void main(String[] args) {

		// Task 1
		System.out.println("Task1:");
		Employee[] employees = { new ComissionEmployee("Jim", "01234", 50000.30),
				new HourlyEmployee("Jerry", "01235", 150.5, 80), new SalariedEmployee("Hans", "01236", 6000.15) };

		System.out.println("This is the employee and salary detail list:");
		System.out.println("--------------------------------------------");
		for (Employee e : employees) {
			System.out.println(e.toString());
		}
		System.out.println("--------------------------------------------");

		Payroll pr = new Payroll(employees);
		pr.paySalary();

		// Task 2
		System.out.println("\n\n\nTask2:");
		System.out.println("--------------------------------------------");
		System.out.println("Why do we need to override hashcode and equals methods:");
		System.out.println(
				"In Java, hash code acts as a key in the hashmap. By default,the system will generate a hashcode for an object basing on its content(value). So when we compare if two objects are equals,");
		System.out.println(
				"1. The system will compare if their hashcodes are the same.If their hashcodes are different, then the system see them as different objects");
		System.out.println(
				"2. If their hashcodes(key) are the same, the system will further call the .equals method to compare their contents(value)");
		System.out.println(
				"3. However, in default method, there's a chance that different objects return with the same hashcode, which means same key with different values.");
		System.out.println(
				"4. To enforce the logic of same hashcode(key) must have same content(key) in a class, and same content(value) must have same hashcode(key), it's better to override both hashcode and equals methods.");
		System.out.println("--------------------------------------------");
		
		// Task 3
		System.out.println("\n\n\nTask3:");
		System.out.println("--------------------------------------------");
		System.out.println("When to we use interface and when do we use abstract classes: ");
		System.out.println("If there's code re-usage between Children classes, use the abstract class: ");
		System.out.println("If there's NO code re-usage between Children classes, use the interface: ");
		System.out.println("--------------------------------------------");
		
		
		// Task 4 Upcasting and Downcasting
		System.out.println("\n\n\nTask4:");
		System.out.println("--------------------------------------------");
		
		System.out.println("Upcasting ");
		Animal a1 = new Dragon();//Upcasting a instance Dragon to it's parent class Animal, acceptable in the complier by default 
		a1.eat();
		a1.run();//By default, this instance can only call methods which have been defined in Animal class. So a1 can call .eat and .run, but it can NOT call .fly.
		//Meanwhile, since it is an Dragon instance, if there's an override method in Dragon, it will execute it instead of the default method in Animal class.
		
		System.out.println("\n\nDowncasting");
		//a1.fly()//this will be a complier error, since the complier is not confident that a1 is an instance of Dragon.
		if (a1 instanceof Dragon) {
			((Dragon) a1).fly();
		} else {
			a1.run();
		}
		
		System.out.println("--------------------------------------------");
	}

}

















//Task 1
class Payroll {
	protected Employee[] employees;

	public Payroll(Employee[] employees) {
		this.employees = employees;
	}

	public void paySalary() {
		for (Employee e : this.employees) {
			System.out.printf("The company just paid salary $%.2f to the employee %s.%n", e.salary(), e.name);
		}
	}
}

abstract class Employee {
	protected String name;
	protected String ssn;

	public Employee(String name, String ssn) {
		this.name = name;
		this.ssn = ssn;
	}

	public abstract double salary();

	public String toString() {
		StringJoiner sj = new StringJoiner(", ");
		sj.add("Name:" + this.name);
		sj.add("SNN:" + this.ssn);
		return sj.toString();
	}
}

class ComissionEmployee extends Employee {
	protected double sales;
	protected double commission;

	public ComissionEmployee(String name, String ssn, double sales) {

		super(name, ssn);
		this.sales = sales;
		this.commission = this.sales * 0.23;
	}

	@Override
	public double salary() {
		DecimalFormat df = new DecimalFormat("#.00");
		double salary = Double.parseDouble(df.format(this.commission));
		return salary;
	}

	public String toString() {
		StringJoiner sj = new StringJoiner(", ", "[", "]");
		sj.add(super.toString());
		sj.add("Sales:" + this.sales);
		sj.add("Commission:" + this.commission);
		sj.add("Salary:" + this.salary());

		return sj.toString();
	}

}

class HourlyEmployee extends Employee {
	protected double wage;
	protected double hours;

	public HourlyEmployee(String name, String ssn, double wage, double hours) {

		super(name, ssn);
		this.wage = wage;
		this.hours = hours;

	}

	@Override
	public double salary() {
		DecimalFormat df = new DecimalFormat("#.00");
		double salary = Double.parseDouble(df.format(this.wage * this.hours));
		return salary;

	}

	public String toString() {
		StringJoiner sj = new StringJoiner(", ", "[", "]");
		sj.add(super.toString());
		sj.add("Wage:" + this.wage);
		sj.add("Hours:" + this.hours);
		sj.add("Salary:" + this.salary());

		return sj.toString();
	}

}

class SalariedEmployee extends Employee {
	protected double basicSalary;

	public SalariedEmployee(String name, String ssn, double basicSalary) {

		super(name, ssn);
		this.basicSalary = basicSalary;

	}

	@Override
	public double salary() {
		DecimalFormat df = new DecimalFormat("#.00");
		double salary = Double.parseDouble(df.format(this.basicSalary));
		return salary;

	}

	public String toString() {
		StringJoiner sj = new StringJoiner(", ", "[", "]");
		sj.add(super.toString());
		sj.add("Salary:" + this.salary());

		return sj.toString();
	}

}


