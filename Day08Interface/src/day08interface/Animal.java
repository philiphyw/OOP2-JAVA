package day08interface;


//Task 4
class Animal{
	
	public void eat() {
		System.out.println("Animal eat");
	}
	
	public void run() {
		System.out.println("Aminal run");
	}
}


class Dragon extends Animal{//why dragon? Because dogs confused me a lot in the on-line class 2 days ago.
	public void run() {
		System.out.println("Dragon run");
		
	}
	
	
	public void fly() {
		System.out.println("Dragon fly");
	}
}