package library;

import java.util.Scanner;

public class Member {
	private int id;
	private String name;
	private double penalty;


	
	public Member(int id, String name) {
		this.setId(id);
		this.setName(name);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPenalty() {
		return penalty;
	}

	public void setPenalty(double penalty) {
		this.penalty = penalty;
	}

	
	Scanner in = new Scanner(System.in);
	public void login() {
		int input=-1;


			do{
				this.displayMenu();
				input = in.nextInt();
				switch (input) {
				case 0:
					System.out.println("Exit system, see you next time.");

					input =0;
					break;
				case 1:
					System.out.println("Call the method to display all available books");
					break;
				case 2:
					System.out.println("Call the method to return a book, but need to find a way to pass parameter");
					break;
				case 3:
					System.out.println("Call the method to reserve a book, but need to find a way to pass parameter");
					break;
				case 4:
					System.out.println("Call the method to checkout a book, but need to find a way to pass parameter");
					break;
				case 5:
					System.out.println("Call the method to cancel a reservation, but need to find a way to pass parameter");
					break;
				case 6:
					System.out.println("Call the method to pay penalty");
					// to-do: need code to provide parameter
					// this.payPenalty();
					break;

				default:
					System.out.println("Invalid action. please re-try.");
				}
			}

			while (input != 0 );
		}


	public void displayMenu() {
		System.out.printf("%n%nHello, our dear member %s, how can we help you?%n", this.getName());
		System.out.println("----------------------------------------------------------");

		StringBuilder menuString = new StringBuilder();
		menuString.append("1. Show all available books %n");
		menuString.append("2. Return a book %n");
		menuString.append("3. Reserve a book %n");
		menuString.append("4. Checkout a book %n");
		menuString.append("5. Cancel a reservation %n");
		menuString.append("6. Pay penalty %n");
		menuString.append("0. Exit %n");

		System.out.printf(menuString.toString());

		System.out.println("----------------------------------------------------------");
	}

}
