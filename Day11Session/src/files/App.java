package files;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class App {
	public static void main(String[] args) {
		try {
			printWriterExample();
			bufferedWriterExample();
			fileOutputStreamExample();
			dataOutputStreamExample();
			
		} catch (IOException e) {
			System.out.println("An exception happened");
		}
	}
	
	
	
	
	private static void bufferedWriterExample() throws IOException{
		String str= "My sentence";
			BufferedWriter writer = new BufferedWriter(new FileWriter("src/resource/testfile1"));
			writer.write(str);
			writer.close();
			System.out.println("bufferedWriterExample is done");
	}
	
	private static void printWriterExample() throws IOException{
		FileWriter fileWriter = new FileWriter("src/resource/testfile2");
		PrintWriter printWriter = new PrintWriter(fileWriter);
		printWriter.println("THis is the expample for printWriter");
		printWriter.println("THis is the second line");
		printWriter.close();
		System.out.println("printWriterExample is done");
	}
	
	
	private static void fileOutputStreamExample() throws IOException {
		String str="This is A string that I need to convert into byte!!!";
		FileOutputStream fileOutputStream = new FileOutputStream("src/resource/testfile3");
		byte[] strToByte = str.getBytes();
		fileOutputStream.write(strToByte);
		fileOutputStream.close();
		
		System.out.println("fileOutputStreamExample is done");
	}
	
	private static void dataOutputStreamExample() throws IOException{
		String str = "my sentence";
		FileOutputStream fos = new FileOutputStream("src/resource/testfile4");
		DataOutputStream outputStream = new DataOutputStream(new BufferedOutputStream(fos));
		outputStream.writeUTF(str);
		outputStream.close();
		
		System.out.println("dataOutputStreamExample is done");
		
	}
	
}


