package clone;

public class App {
 public static void main(String[] args) {
	 Employee emp1 = new Employee("Timmy");
	 
	 try {
		Employee emp2 = (Employee)emp1.clone();
		System.out.println(emp2.getName());
		
	} catch (CloneNotSupportedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
 }
}




class Employee{
	private String name;
	public Employee(String name) {
		setName(name);
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public Object clone() throws CloneNotSupportedException{
	 return super.clone();
	}
}