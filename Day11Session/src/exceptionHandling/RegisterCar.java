package exceptionHandling;

class Car {
	private String make;
	private String year;

	public Car(String make, String year) {
		setMake(make);
		setYear(year);
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		if (year.startsWith("20") && year.length() == 4) {
			this.year = year;
		} else {
			throw new IllegalArgumentException("This year needs to be after 2000");
		}

	}
}


public class RegisterCar{
	public void carFunctions() {
		try {
			Car generatedCar = generateCar("Honda","20AC");
			registerNewCar(generatedCar);
		} catch (IllegalArgumentException e) {
			System.out.println("An error happened " + e.getMessage());
		}
		

		
	}
	
	/**
	 * 
	 * @param make
	 * @param year
	 * @return Car object
	 * @throws IllegalArgumentException then the year is not in a correct format/range
	 */
	
	public Car generateCar(String make, String year) throws IllegalArgumentException {
		return new Car(make,year);
	}
	public void registerNewCar(Car car) {
		System.out.println("the car is registered");
	}
}