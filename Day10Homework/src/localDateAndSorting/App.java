package localDateAndSorting;

import java.util.List;

public class App {
	public static void main(String[] args) {
		List<User> users = User.initializeUsers();
		
		System.out.println("Before sorting");
		users.forEach(e->System.out.println(e.toString()));
		
		
		System.out.println("\n\nAfter sorted by id");
		User.sortListByType(users,	 1);
		users.forEach(e->System.out.println(e.toString()));
		
		System.out.println("\\n\\nAfter sorted by name");
		User.sortListByType(users,	 2);
		users.forEach(e->System.out.println(e.toString()));
		
		System.out.println("\\n\\nAfter sorted by birthdate");
		User.sortListByType(users,	 3);
		users.forEach(e->System.out.println(e.toString()));
		
		
		
		
		
		
	}
	
	

}