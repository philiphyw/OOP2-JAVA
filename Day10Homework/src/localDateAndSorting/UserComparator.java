package localDateAndSorting;

import java.util.Comparator;

class UserIDComparator implements Comparator<User> {
	@Override
	public int compare(User u1, User u2) {
		if(u1.getId()<u2.getId()) {
			return -1;
		}else if(u1.getId()==u2.getId()) {
			return 0;
		}else{
			return 1;
		}
	}
}


class UserBirthDateComparator implements Comparator<User> {
	@Override
	public int compare(User u1, User u2) {
		if(u1.getBirthDate().isBefore(u2.getBirthDate())) {
			return -1;
		}else if(u1.getId()==u2.getId()) {
			return 0;
		}else{
			return 1;
		}
	}
}

class UserNameComparator implements Comparator<User> {
	@Override
	public int compare(User u1, User u2) {
		return u1.getName().compareTo(u2.getName());
	}
}