package localDateAndSorting;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IllegalFormatException;
import java.util.List;

public class User {
	private int id;
	private String name;
	private LocalDate birthDate;

	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public User(int id, String name, LocalDate birthDate) {
		try {
			this.birthDate = birthDate;
		} catch (IllegalFormatException e) {
			System.err.println("The birthDate data type needs to be a LocalDate. " + e.getMessage());
		}
		
		if (name == null) {
			System.err.println("THe name can NOT be Null");
		}else {
			this.name = name;
		}
		this.id=id;
	}
	
	public static List<User> initializeUsers() {
		List<User> users = new ArrayList<User>();
		users.add(new User(191,"Jim",LocalDate.parse("2001-12-06")));
		users.add(new User(202,"Tommy",LocalDate.parse("1975-01-08")));
		users.add(new User(108,"Keivn",LocalDate.parse("1964-08-23")));
		users.add(new User(104,"Steve",LocalDate.parse("1976-08-22")));
		users.add(new User(680,"Johnny",LocalDate.parse("1993-07-14")));
		
		return users;
	}
	
	@Override
	public String toString() {
		return "[ id= " + this.id + ", name= " + this.name + ", birthday= " + this.birthDate.toString() + " ]";
	}
	
	
	
	public static void sortListByType(List<User> users, int type) {
		switch (type) {
		case 1:
			Collections.sort(users, new UserIDComparator());
			break;
		case 2:
			Collections.sort(users, new UserNameComparator());
			break;
		case 3:
			Collections.sort(users, new UserBirthDateComparator());
			break;

		default:
			System.out.println("The type needs to be 1,2 or 3;");
			break;
		}
	}	
}
