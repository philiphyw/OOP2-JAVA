package exceptonhandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
//		Scanner input = new Scanner(System.in);
//		System.out.println("Please input the first number: ");
//		int number1 = input.nextInt();
//		System.out.println("Please input the second number: ");
//		int number2 = input.nextInt();
//		input.nextLine();
//		
//		try {
//		int result = quotient(number1, number2);
//		System.out.println("The result of number1/number2 is " + result);
//		}catch(ArithmeticException e) {
//			//System.out.println("Exception: the second number cannot be zero.");
//			System.out.println(e.getMessage());
//		}
//
//		System.out.println("Execution continues...");
//		
//		inputInteger();

//		
//		double d= 1.0/0;
//		System.out.println(d);
//		
//		long l = Long.MAX_VALUE +1;
//		System.out.println(Long.MIN_VALUE);
//		System.out.println(l);
//		for (int i = 0; i < 2; i++) {
//			System.out.print(i + " ");
//			try {
//				System.out.println(1 / 0);
//			} catch (Exception ex) {
//			}
//		}
//
//		try {
//			for (int i = 0; i < 2; i++) {
//				System.out.print(i + " ");
//				System.out.println(1 / 0);
//			}
//		} catch (Exception ex) {
//		}

		System.out.println(averageThree());
		
		
	}

	public static int quotient(int number1, int number2) {
		if (number2 == 0) {
			throw new ArithmeticException("The divisor cannot be zero");
		}
		return number1 / number2;
	}

	public static void inputInteger() {
		Scanner sc = new Scanner(System.in);
		boolean continueInput = true;
		do {
			try {
				System.out.println("Please input an integer");
				int input = sc.nextInt();
				System.out.println("The number you inputed is " + input);
				continueInput = false;
			} catch (InputMismatchException e) {
				System.out.println("The content you inputed is NOT an integer, please retry.");
				sc.nextLine();
			}
		} while (continueInput);
	}

	
	public static double averageThree() throws IllegalArgumentException{
		int a;
		int b;
		int c;
		
		Scanner input = new Scanner(System.in);
		System.out.println("Input the first number: ");
		a = input.nextInt();
		System.out.println("Input the second number: ");
		b = input.nextInt();
		System.out.println("Input the third number: ");
		c = input.nextInt();
		
		input.close();
		return ((double)(a+b+c))/3;
		
	}
}
