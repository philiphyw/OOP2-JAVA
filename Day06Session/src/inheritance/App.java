package inheritance;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Machine m1 = new Machine();
		m1.start();
		Car m2 = new Car();
		m2.start();
		m2.start(5);
		
		
		
		Tree rw=new RedWood();
		rw.growUp();
		//rw.getRedder()//compile error, since rw is an instance of Tree class, not an instance of RedWood class
		RedWood rw2= new RedWood();
		
		
		

	}

}
