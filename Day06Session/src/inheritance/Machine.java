package inheritance;

class Machine {
	private String name;
	private double weight;

	public void start() {
		System.out.println("Machine started");
	}
	
}

class Car extends Machine {

	public void start() {
		System.out.println("Car started");
	}
	
	
	public void start(int times) {
		String plural=(times >=2)?"s":"";
		System.out.println(String.format("Car started %d time%s", times,plural ));
	}
}


abstract class Tree{
	
	public void growUp() {
		System.out.println("Tree is growing up");
	};
}


class RedWood extends Tree{
	
	public void growUp() {
		System.out.println("Redwood is growing up");
	}
	
	public void turnReder() {
		System.out.println("Redwood is getting redder.");
	}
}