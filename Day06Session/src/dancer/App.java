package dancer;

public class App {

	public static void main(String[] args) {
		Dancer [] dancers = {
				new Dancer(),
				new ElectricBoogieDancer(),
				new Breakdancer()
		};

		for(Dancer d: dancers) {
			d.dance();
		}
		
	}

}


 class Dancer{
	
	public void dance() {
		System.out.println("There's a dancer, the dancer dances");
	}
	
}



class ElectricBoogieDancer extends Dancer{
	
	@Override
	public void dance() {
		System.out.println("Check it out, the Electric Boogie Dancer is dancing.");
	}
	
}
class Breakdancer  extends Dancer{
	
	@Override
	public void dance() {
		System.out.println("Cool, a Break Dancer is dancing.");
	}
	
}
