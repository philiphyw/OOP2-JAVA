package filehandling;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class App {
	public static void main(String[] args){
		try {
		File sourceFile = new File("D:/JAVA OOP2 Gitlab Workspace/Day16FileHandling/src/filehandling/sourceFile.txt");
		System.out.println("Is the file eixted? " + sourceFile.exists());
		boolean isExisted = sourceFile.exists();
		if (isExisted == false) {
		
			sourceFile.createNewFile();
			System.out.println("Creating file...");
		}
		
		System.out.println("Is the file eixted now? " + sourceFile.exists());
		System.out.println("Is the file a folder? " + sourceFile.isDirectory());

		String fileName = sourceFile.getName();
		System.out.println("What's the name of the file? " + fileName);
		System.out.println("What's the absolute path of the file? " + sourceFile.getAbsolutePath());
		System.out.println("What's the size of the file? " + sourceFile.length());
		
		//check if the file is editable
		boolean isEditable = sourceFile.canWrite();
		
	
		System.out.println("Is the file editable? " + sourceFile.canWrite());
		
		String filePath = sourceFile.getAbsolutePath().replace("\\", "/");//needs to replace the default path separator \ to the java syntax /;
		PrintWriter pw = new PrintWriter(new FileWriter(filePath));
		for (int i = 0; i < 10; i++) {
		pw.println("This is the line " + i + ":         " + (i + Math.random()) * Math.random() );
		
		}
		pw.close();
		System.out.println("Writing the file...");
		System.out.println("What's the size of the file now? " + sourceFile.length());
		
		
		File sourceFolder = new File("D:\\Livres\\Programming\\Git\\Udemy - Learn Git From Scratch 2020\\4. Git Branches and GitHub");
		
		File [] files = sourceFolder.listFiles();
		System.out.println("How many files in this folder: " + files.length);
		for (int i = 0; i < files.length; i++) {
			String fileOldName = files[i].getAbsolutePath();
			String deleteString = "--- [ FreeCourseWeb.com ] ---";
			if (fileOldName.contains(deleteString)) {
				String newFileName=fileOldName.replace(deleteString, "");
				files[i].renameTo(new File(newFileName));
				System.out.println("file is renamed");
			}
		}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
}
