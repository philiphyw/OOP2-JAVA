package day3datatype;

import java.util.Arrays;

public class ClassRefValueType {

	public static void main(String[] args) {
		String [] memberArr = new String[] {"Jim","Kim","Tim"};
		int[] codeArr = new int [] {50,60,70};
		
		
		/*
		 * member1 use the constructor which ref the external String[] and int[], once the array change, the content in member will also change
		 * member2 use the constructor which copy value from the external String[] and int[], once the array change, the content in member won't also change
		 
		 */
		Member member1 = new Member(memberArr, codeArr);
		Member member2 = new Member(memberArr, codeArr,true);
		
		System.out.println("Before change, member 1 and member2: ");
		member1.displayDetail();
		member2.displayDetail();
		
		
		System.out.println("After change, member 1 and member2: ");

		memberArr[1] = "Kimmy";
		codeArr[1]=999;
		
		member1.displayDetail();
		member2.displayDetail();
		
		
		
		
		
		
	}

}


class Member{
	protected String[] members;
	protected int[] codes;
	
	
	Member(){};
	
	Member(String[] members, int[] codes){
		this.members = members;
		this.codes=codes;
	}
	
	
	Member(String[] members, int[] codes, boolean valueType){
		String[]  tempStr = new String[members.length];
		for (int i = 0; i < tempStr.length; i++) {
			tempStr[i] = members[i];
		}
		
		this.members = tempStr;
		
		int [] tempInt= new int [codes.length];
		for (int i = 0; i < tempInt.length; i++) {
			tempInt[i] = codes[i];
		}
		
		this.codes=tempInt;
	}
	
	public void displayDetail() {
	 	
    System.out.println("Members: " + Arrays.toString(members));
    System.out.println("Codes: " + Arrays.toString(codes));
		
	}
	
	
}