package day02homework;

//Assignment 1
class Person {
	private String firstName;
	private String lastName;
	private int age;

	Person() {
	};

	Person(String firstName, String lastName, int age) {
		this.firstName = firstName;
		this.lastName = lastName;
		setAge(age);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		if (age < 0 || age > 100) {
			this.age = 0;
		} else {
			this.age = age;
		}

	}

	public boolean isTeen() {
		if (this.age > 12 && this.age < 20) {
			return true;
		} else {
			return false;
		}
	}

	public String getFullName() {

		if (this.firstName.isEmpty() && this.lastName.isEmpty()) {
			return "";
		} else if (this.firstName.isEmpty() && !(this.lastName.isEmpty())) {
			return this.lastName;
		} else if (!(this.firstName.isEmpty()) && this.lastName.isEmpty()) {
			return this.firstName;
		} else {

			return this.firstName + " " + this.lastName;
		}

	}

	public void displayProfile() {
		System.out.println("fullName= " + this.getFullName());
		System.out.println("teen = " + this.isTeen());
	}
}

//Assignment 2

class SimpleCalculator {
	private double firstNumber;
	private double secondNumber;

	public void setFirstNumber(double number) {
		this.firstNumber = number;
	}

	public void setSecondNumber(double number) {
		this.secondNumber = number;
	}

	public double getFirstNumber() {
		return this.firstNumber;
	}

	public double getSecondNumber() {
		return this.secondNumber;
	}

	public double getAdditionResult() {
		return this.firstNumber + this.secondNumber;
	}

	public double getSubtractionResult() {
		return this.firstNumber - this.secondNumber;
	}

	public double getMultiplicationResult() {
		return this.firstNumber * this.secondNumber;
	}

	public double getDivisionResult() {
		if (this.secondNumber == 0.0) {
			return 0.0;
		} else {
			return this.firstNumber / this.secondNumber;
		}

	}

}

//Assignment 3

class Wall {
	private double width;
	private double height;

	Wall() {
	}

	Wall(double width, double height){
		this.setWidth(width);
		this.setHeight(height);
	}

	public double getWidth() {

		return width;
	}

	public void setWidth(double width) {
		if (width <= 0) {
			this.width = 0;
		} else {
			this.width = width;
		}
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		if (height <= 0) {
			this.height = 0;
		} else {
			this.height = height;
		}

	}
	
	
	public double getArea() {
		return this.width * this.height;
	}

}

public class Day02Homework {

	public static void main(String[] args) {
		// Test Assignment 1
		System.out.println("Test result of assignment 1:");
		Person person = new Person("", "", 10);
		person.displayProfile();
		person.setFirstName("John");
		person.setAge(18);
		person.displayProfile();
		person.setLastName("Smith");
		System.out.println("fullName= " + person.getFullName());

		// Test Assignment 2
		System.out.println("\n\nTest result of assignment 2:");
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4.0);
		System.out.println("add= " + calculator.getAdditionResult());
		System.out.println("subtract= " + calculator.getSubtractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply= " + calculator.getMultiplicationResult());
		System.out.println("divide= " + calculator.getDivisionResult());

		// Test Assignment 3
		System.out.println("\n\nTest result of assignment 3:");
		Wall wall = new Wall(5,4);
		System.out.println("area= " + wall.getArea());
		
		wall.setHeight(-1.5);
		System.out.println("width= "+wall.getWidth());
		System.out.println("height= "+wall.getHeight());
		System.out.println("area= "+wall.getArea());
	}

}
