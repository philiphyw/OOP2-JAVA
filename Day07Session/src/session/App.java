package session;

public class App {

	public static void main(String[] args) {

//		Person p = new Person("Jim",1522301);
//		
//		Student s = new Student("Kim",709394,5544331);
//
//		
//		System.out.println("the default toString method (package name + class name + hashcode) returns :" +  p.toString());
//		System.out.println("the override toString method returns :" + s.toString());
//		
//		//whenever a child class is being instantiated , its parent class constructor will be called first.
//		Teacher t = new Teacher();
//		
//		Person p1 = t;
//		
//		Person p2 = new Teacher();
//		
//		p1.run();
//		p2.run();
//		
//		System.out.println(); 
//		youRun(t);
//		youRun(s);
//		
//		
//		
//		Teacher t1 = (Teacher)new Person("John",12344);
//		t1.run();
		
		
		//Casting up, casting down
		Truck t1 = new Truck();
		t1.run();
		Car c1= new Car();
       //downcasting  is acceptable by complier by default, since Children class includes(by inheritance) all parent class's field/method		
		c1 = t1;
		c1.run();
		
		
		
		//upcasting is NOT acceptable by complier by default, since parent class may NOT includes certain fields/methods in children class
		//t1=(Car)c1;//compile error
		Car c2 = new Truck();
		t1 = (Truck)c2;//c2 belongs to parent class Car and point to a child class instance Truck, use (Truck) to force downcasing;

		
		Car c3 = new Car();
		Car c4 = new Truck();
		
		System.out.println("Is the Car c3 <Car c3 = new Car()> able to downcast to a Truck: " + canDowncastToTruck(c3)); 
		System.out.println("Is the Car c4 <Car c4 = new Truck()> able to downcast to a Truck: " + canDowncastToTruck(c4)); 
		
	}

	//to ensure downcasting without any compile error, using the method instanceOf() to check which class the instance is actually point to
public static boolean canDowncastToTruck(Car car) {
		if (car instanceof Truck) {
			return true;
		} else {
			return false;
		}
}


public static void youRun(Person p) {
	p.run();
}
}