package session;

import java.util.StringJoiner;

class Person {

	protected String name;
	protected long id;
	
	public Person(){
		System.out.println("A Person constructor has been called");
	}
	
	public Person(String name, long id){
		this.name=name;
		this.id=id;
	}

	public void run() {
		System.out.println("Person runs");
	}
}


class Student extends Person{
	final long studentCode;
	

	
	
	public Student(String name, long id, long studentCode){
		super(name,id);
		this.studentCode=studentCode;
	}
	
	//the default toString method will return the hash code of the instance, override the default toString method
	public String toString() {
		StringJoiner sj = new StringJoiner(",","Student info: ","!");
		
		sj.add(this.name);
		sj.add(Long.toString(this.id));
		sj.add(Long.toString(this.studentCode));
		
		return sj.toString();
	}
	
	
	public void run() {
		System.out.println("Student runs");
	}
}


class Teacher extends Person{
	
	public Teacher(){
	
		System.out.println("A Teacher constructor has been called");
		
	}
	
	public void run() {
		System.out.println("Teacher runs");
	}
	
}