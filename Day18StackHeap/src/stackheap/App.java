package stackheap;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int speedLimit = 120;
		
		Car myCar = new Car();
		myCar.speed=100;
		
		Engine v8 = new Engine();
		v8.horsePower=500;
		
		myCar.engine=v8;
		
		System.out.printf("\nMy car speed is %d, its engine power is %d.\n",myCar.speed,myCar.engine.horsePower);
		
		
		myCar=null;
		
		
	}

}
