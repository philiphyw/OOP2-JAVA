package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class App {

	public static void main(String[] args) {
//Array fixed length while declaring the object, unable to add/remove element.
		String[] strArr = { "Cats", "Cats", "Dogs", "Birds", "Dogs" };
		System.out.println("The content in the Array: " + Arrays.toString(strArr));

		// List flexible size, able to add/insert/remove element, allow duplicate
		// elements
		List<String> strList = new ArrayList<String>();// no need to declare the size(quantity of elements)
		for (String s : strArr)
			strList.add(s);
		System.out.println("\nList content before inserting element:");
		strList.forEach(element -> System.out.print(element.toString() + " "));
		strList.add(2, "Cats");
		System.out.println("\nList content after inserted element:");
		strList.forEach(element -> System.out.print(element.toString() + " "));
		Collections.sort(strList);
		System.out.println("\nList content after sorting:");
		strList.forEach(element -> System.out.print(element.toString() + " "));
		Collections.
	}

}
