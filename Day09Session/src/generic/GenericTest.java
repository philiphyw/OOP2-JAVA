package generic;

import java.util.ArrayList;
import java.util.List;

class GenericTest {
	public static void main(String[] args) {
		// Generic method can NOT accept primitive variable as parameter, it only accept
		// reference type variable
		Test<String> testString = new Test<String>("First string");
		System.out.println(testString.getObject());

		Test<Integer> testInteger = new Test<Integer>(12);
		System.out.println(testInteger.getObject());

	}

//Generic class and generic method.
//Generic method can NOT accept primitive variable as parameter, it only accept reference type variable
	Test<String> testString = new Test<String>("First string");

}


class Test<E>{
	E obj;
	
	public Test(E obj) {
		this.obj = obj;
	}
	
	
	public E getObject(){
		return this.obj;
	}
}