package collectionexamples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionExamples {
	public static void main(String[] args) {

		// List
		System.out.println("\n\n\n Examples for Set");
		List<Integer> listOfInteger = new ArrayList<Integer>();// The List is an interface and can NOT be instanized,

		for (int i = 0; i < 10; i++) {
			int number = (int) (Math.random() * 100) + 1;
			listOfInteger.add(number);
		}

		System.out.println(listOfInteger.toString());

		for (Integer integer : listOfInteger) {
			System.out.print(integer + " ");
		}

		// java 9 foreach and lambda
		listOfInteger.forEach(element -> System.out.println(element + " "));

		String[] stringArr = { "Jim", "Tom", "Pam" };
		List<String> listOfString = Arrays.asList(stringArr);

		listOfString.forEach(str -> System.out.print(str + " "));

		// Set
		System.out.println("\n\n\n Examples for Set");
		Set<Integer> setOfInteger = new HashSet<Integer>();
		setOfInteger.add(1);
		setOfInteger.add(2);
		setOfInteger.add(2);
		setOfInteger.add(2);
		setOfInteger.add(3);

		// Set does NOT allow duplicate values, so below method will print out each
		// unique element
		setOfInteger.forEach(element -> System.out.print(element + " "));

		// Map
		System.out.println("\n\n\n Examples for Map");
		
		Map<String, Address> employees = new HashMap<String, Address>();
		String[] nameArr = { "Jim", "Tom", "Pam" };
		Address[] addressArr = {new Address("Graham","H3P2C7"), new Address("Pointe Claire","456202"), new Address("John-Talon","G213")};
		
		for (int i = 0; i < nameArr.length; i++) {
			employees.put(nameArr[i], addressArr[i]);
		}
		
		//get the key and value by loop the entrySet()
		for (Map.Entry<String, Address> entry : employees.entrySet()) {
			System.out.println(entry.getKey() + ":" + entry.getValue());
			
		}
	}
}



class Address{
	protected String streetName;
	protected String postalCode;
	
	Address(String streetName, String postalCode){
		this.streetName = streetName;
		this.postalCode= postalCode;
		
	}

	public String getStreetName() {
		return streetName;
	}

	public String getPostalCode() {
		return postalCode;
	}
	
	//need to override the toString method to return meaningful value to the Map.entrySet();
	@Override
	public String toString() {
		return this.streetName + " " + this.postalCode;
	}
	
}
