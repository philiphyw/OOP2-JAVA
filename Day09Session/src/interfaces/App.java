package interfaces;

//add this line to test merge from master to dev


//Following activities are in SourceTree
//1. Checkout master;
//2. Pull the change;
//3. Check out dev
//4. rightclick the master tab, select merge to dev(or any other branch name you're working on)

public class App {
	public static void main(String[] args) {
		
		Member m1 = new Member();
		VIP v1 = new VIP();
		
		m1.selfIntro();
		v1.selfIntro();
		
	}
}
