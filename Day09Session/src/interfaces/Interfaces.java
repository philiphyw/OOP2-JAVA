package interfaces;

interface IRegister extends IManage, IAdmin{
//if we have a final variable in a class, we need to initialize them
	final static int myID = 1234;
	final String MY_CODE = "AH2301";//making a final String capitalize is a coding convention, just like a constant.
	
	void register();
	
	
	//After JAVA 8, with the default keyword, interface can basically replace abstract class
	default void selfIntro() {
		System.out.println("I am a register method");
	}
	
}


interface IManage{
	void manage();
}

interface IAdmin{
	void updateProfile();
}



class Member implements IRegister{	
	
	@Override
	public void register() {
		// TODO Auto-generated method stub
	
		
		


		
	}
	
	//Following 2 methods are NOT in IRegister, but they are in other 2 ancestor interfaces IManage, IAdmin, any class implements IRegister also need to override all methods in ancestors interface 

	@Override
	public void manage() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateProfile() {
		// TODO Auto-generated method stub
		
	}
	
}


class VIP implements IRegister{
	public void selfIntro() {
		System.out.println("I am a register method for VIP only yo.");
	}

	@Override
	public void manage() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateProfile() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void register() {
		// TODO Auto-generated method stub
		
	}
	
	
	
}