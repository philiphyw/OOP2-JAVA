package session2;

import java.util.Arrays;

class Car {
	private Owner owner;
	private String name;
	private int maxSpeed;

	Car() {
	}

	Car(String name, int maxSpeed){
		this.name = name;
		this.maxSpeed = maxSpeed;
	}

	Car(Owner owner,String name, int maxSpeed) {
		this(name,maxSpeed);
		this.owner = owner;
	}

	public void displayDetial() {
		String s = String.format("This is a car named %s, it belongs to %s, its max speed is %d. %n", this.name,
				this.owner.getName(), this.maxSpeed);
	}
}

class Owner {
	private String name;
	private Car[] car;
	private int ownedCar = 0;

	Owner() {

	}

	Owner(String name, String carName, int maxSpeed) {
		this.name = name;
		Car car = new Car(this, carName, maxSpeed);
		this.car[ownedCar] = car;
		this.ownedCar++;

	}

	public void addCar(Car car) {
		this.car[ownedCar] = car;
		this.ownedCar++;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCar() {
		return Arrays.toString(this.car);
	}

	public int getOwnedCarQuantity() {
		return ownedCar;
	}




	
}

public class This {
	public static void main(String[] args) {
       Owner owner1 = new Owner("Jim", "Tesla Model 3", 150);
       owner1.getOwnedCarQuantity();
	}
}
