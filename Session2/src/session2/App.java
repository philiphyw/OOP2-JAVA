package session2;




class Person{
	String name;
	
	
	Person(){}
	
	Person(String name){
		this.name = name;
	}
}

class Test{
	
	
	public void mymethod(int x) {
		x=20;
	}
	
	public void mymethod2(Person p) {
		p.name = "Roger";//it didn't change the value in the String in p.name, instad, it create a new String and assign to p.name
	}
	
	public void mymethod3( String str) {
		str="Roger";
	}
	
	
	public  void testCallByValue() {
	      int x=5;
	      System.out.println("The value of X = " + x);
	      mymethod(x);
	      System.out.println("The value of X = " + x);
	      
	}
	
	
	public void testCallByReference() {
		
		String initialName= "Klaus";
		Person person1= new Person(initialName);
		Person person2 = new Person(initialName);
		
		System.out.println("The value of name = " + person1.name);
		//Person, as an non primitive object, it's a reference type.
		mymethod2(person1);
		System.out.println("The value of name = " + person1.name);
		
		
		
		//String is immutable, so it will never be changed, so the toUpperCase and contact will NOT change the value in person.name;
		mymethod3(initialName);
		person2.name.toUpperCase();
		person2.name.concat("lalalalalal");
		person2.name.replace("o", "Q");
		System.out.println("The value of name = " + person2.name);
		
	}
	
	
}


public class App {
	public static void main(String[] args) {

		Test test = new Test();
		test.testCallByValue();
		
		test.testCallByReference();
	}
}
