package classbasic;

public class InnerNestedClass {
	public static void main(String[] args) {

		Outer outerOne = new Outer("outerOne");
		Outer.Inner nestedInner = new Outer.Inner();
		
		Outer.run();//static method, need to use class name to call
		outerOne.runAgain();//non static method, need to use instance to call
		
		Outer.Inner.innerRun();//Static method, need to use class to call
		nestedInner.innerRunAgain();//non static method, need to use instance to call
		nestedInner.innerRunAgain();//non static method, need to use instance to call
	}
}



class Outer{
	private static String staticName = "A Static Outer String";
	
	public static int countOuter=0;
	private String name;//this is non static field, so the inner class can NOT access.
	
	Outer(){}
	
	Outer(String name){
		this.name=name;
	}
	
	static void run() {
		System.out.println("Outer run");
	}
	
	void runAgain() {
		System.out.println("Outer run agian");
	}
	
	
	static class Inner{
		
		static void innerRun() {
			System.out.println("Inner run");
		}
		
	    void innerRunAgain() {
	    	countOuter++;
	    	System.out.println("Inner run agian, how about you? "+ Outer.staticName + countOuter);
	    	
	    }
	}
	
}