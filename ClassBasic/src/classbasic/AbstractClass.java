package classbasic;


abstract class Money{
   protected double amount;
   
   Money(){};
   
   Money(double amount){
	   this.amount=amount;
   }
   
   
   abstract double calTax(); 
}


class MonthlySalary extends Money{
	
	MonthlySalary(){}
	
	MonthlySalary(double amount){
		super(amount);
	}
	
	@Override
	public double calTax() {
		if (this.amount <= 5000) {
			return 0.0;
		} else {
			return (this.amount - 5000)* 0.2;
		}
	}
	
}


class TaxFreeIncome extends Money{
	TaxFreeIncome(){}
	
	TaxFreeIncome(double amount){
		super(amount);
	}
	
	
	@Override
	public double calTax() {
		return 0.0;
	}
}


class Lottery extends Money{
	
	Lottery(){}
	
	Lottery(double amount){
		super(amount);
	}
	
	@Override
	public double calTax() {
		return this.amount * 0.4;
	}
	
}


public class AbstractClass {

	public static void main(String[] args) {
       
		Money[] moneys = {
				new MonthlySalary(35000),
				new TaxFreeIncome(10000),
				new Lottery(20000)
				
		}; 
		
		double totalTax= 0.0;
		
		for(Money money : moneys) {
			
			totalTax += money.calTax();
		}

		System.out.println("The total tax is " + totalTax);
		
	}

}
