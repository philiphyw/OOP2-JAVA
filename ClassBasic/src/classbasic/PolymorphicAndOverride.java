package classbasic;

class Income {

	protected double amount;

	Income() {
	}

	Income(double amount) {
		this.amount = amount;

	}

	// the calTax will be overrided by Children class
	public double calTax() {

		return amount * 0.1;

	}

}

class Salary extends Income {

	public Salary(double amount) {
		super(amount);
	}

	@Override // tax rate will be different from Parent class Income;
	public double calTax() {
		if (this.amount <= 5000) {
			return 0.0;
		} else {

			return (this.amount - 5000) * 0.2;

		}
	}

}

class TFSA extends Income {
	public TFSA(double amount) {
		super(amount);
	}
	
	
	@Override
	public double calTax() {
		return 0.0;
	}

}

public class PolymorphicAndOverride {

	public static void main(String[] args) {
 
		Income[] incomes = new Income[3];
		incomes[0] = new Income(10000);
		incomes[1] = new Salary(15000);
		incomes[2] = new TFSA(10000);
		
		double totalTAX = calTotalTax(incomes);
		
		System.out.printf("The total tax for normal income, salary isand the TFSA is %.2f: %n ", totalTAX );
	}

	private static double calTotalTax(Income[] incomes) {
		// TODO Auto-generated method stub
		double totalTax=0.0;
		for (Income i: incomes) {
			totalTax += i.calTax();
		}
		return totalTax;
	}
}
