package classbasic;

class User {

	private String username;
	private int code;
	private String userEmail;
	
	private static int activeUser=0;

	
	
	
	
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	
	public static int countActiveUser() {
		return activeUser;
	}
	
	User() {
		activeUser++;
	}

	User(String name, int code, String email) {
		this.setUsername(name);
		this.setCode(code);
		this.setUserEmail(email);
		
		activeUser++;
	}

}


class UserManagement{
	
	public void manageUser(User user) {
		System.out.printf("Now we are managing the user %s, whose code is %d.%n", user.getUsername(),user.getCode());
	}
	
}

public class ClassBasic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		User stewie = new User("Stewie Griffin",101,"sgffin@fg.com");
		UserManagement um = new UserManagement();
		um.manageUser(stewie);
		
		User brian = new User();
		System.out.println("Current active user number is " + User.countActiveUser());
		
		User peter = new User();
		System.out.println("Current active user number is " + User.countActiveUser());
		
		
		
		//inheritance
		VIP roger = new VIP("Roger Smith",9394,"rsmith@ad.com");//call one of the parameter-included constructor which has been defined in the parent class;
		roger.showOff();
		
		System.out.println("Current active user number is " + User.countActiveUser());
		
	}

}
